from django import template

register = template.Library()


# {{ ingredient.amount|resize_to:servings }}
@register.filter
def resize_to(ingredient, serving):
    ratio = float(serving) / ingredient.recipe.servings
    return float(ingredient.amount) * float(ratio)
