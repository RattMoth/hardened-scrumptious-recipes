from recipes.models import ShoppingItem


def shopping_count(request):
    # return {"shopping_count": ShoppingItem.objects.filter(user=request.user)}
    shopping_count_list = []

    for item in ShoppingItem.objects.filter(user=request.user):
        shopping_count_list.append(item.food_item)

    return {"shopping_count": shopping_count_list}
