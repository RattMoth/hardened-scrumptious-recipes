from django import forms

from recipes.models import Rating, FoodItem


class RatingForm(forms.ModelForm):
    class Meta:
        model = Rating
        fields = ["value"]


class FoodItemForm(forms.ModelForm):
    class meta:
        model = FoodItem
        fields = [""]
