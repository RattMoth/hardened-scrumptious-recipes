from django.contrib import admin

from tags.models import Tag
from meal_plans.models import MealPlan


# Register your models here.
class TagAdmin(admin.ModelAdmin):
    pass


class MealPlanAdmin(admin.ModelAdmin):
    pass


admin.site.register(Tag, TagAdmin)
admin.site.register(MealPlan, MealPlanAdmin)
