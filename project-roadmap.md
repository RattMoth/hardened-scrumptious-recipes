Author: Matt Roth  
Date: Wed-8-Jun-2022  
Subject: Scrumptious Practice Project Roadmap

---

# Steps to Scrumptious Meal Plans

- [x] Create app `meal_plans`
  - Add missing `urls.py`
- [x] Create 2 new users in admin panel
  - pass: `mygoodpassword`
  - `SuperUser: 1111`
- [x] Create `MealPlan` model
  - [x] Name: string
  - [x] Date: date
  - [x] Owner: `AUTH_USER_MODEL`
  - [x] Recipes: m2m Recipe model (w/ related name = "recipes")
- [ ] Create views (remember `LoginRequiredMixin`):

  - List

    - [x] Only show user's plans
    - [x] Page should include link to create

    - [x] Name of each plan should be a link to its detail view

  - Create
    - [x] show form w/ name, date, recipe selector
    - [x] owner property auto set to user
    - [x] redirect to detail page for new plan
    - [ ] requires extra sumn-sumn to ensure recipes are saved with the meal plan (`save_m2m` method)
      - It seems like this _might_ not be necessary. Perhaps this behavior is handled by Django's `ListView`?
  - Detail
    - [x] shows name, date it is to be served, and list of recipes associated with it. each name should link to recipe page
    - [x] page should have a link to edit and delete
    - [x] If not auth, no meal plan. Yellow Page of Sad is acceptable here
  - Edit
    - [x] Should be very similar to delete and detail views
  - Delete
    - [x] show form that allows owner to delete
    - [x] redirect to list view
    - [x] YPoS if not auth

- [x] Create paths (5):
  - [x] meal_plans/ list of a user's plans
  - [x] meal_plans/create/ allows user to create plan
  - [x] meal_plans/<int:pk> show detail of plan (if auth)
  - [x] meal_plans/<int:pk>/edit edit plan (if auth)
  - [x] meal_plans/ <int:pk>/delete delete (if auth)
- [x] Create/style templates
  - [x] Should extend `base.html`
  - [x] Update page titles to reflect content

# Steps to Scrumptious Shopping Lists

Date: Thur-9-Jun-2022

**Overview**: Users will be able to create a shopping list from recipes.
Clicking an ingredient's "add to list" button will place that ingredient in the
user's shopping list, if it isn't already. A link in the top navbar will show
the current number of items, as well as link to the shopping list. **This feature should live in the** `recipes` **app.**

**Special Considerations**: Two features will require the use of function views:

1. Our desired behavior for the _delete view function_ needs to delete _all_
   items. `DeleteView` only really handles one at a time.
2. Our view that _adds an ingredient's food item to the shopping list_ doesn't
   show a standard form like other update views.

**MVP:**

1. A "+shopping list" button next to any ingredient _not already in the shopping list_. When clicked, adds item to shopping list.
2. A main nav link that links to a list of the current shopping list, with an indicator of how many items it contains
3. The actual shopping list of food items
4. A button on the list page that clears all items

## Roadmap

- [ ] Create `ShoppingItem` model. User should only be able to add a food item once to their list.
  - [x] user = `AUTH_USER_MODEL` : foreign key to user model. Cascade on delete
  - [x] food_item = `FoodItem` : foreign key. Protect on delete
  - [x] Handle migrations
  - [x] Revisit models at the end got this warning

```
recipes.ShoppingItem.food_item: (fields.W342) Setting unique=True on a ForeignKey has the same effect as using a OneToOneField.
        HINT: ForeignKey(unique=True) is usually better served by a OneToOneField.
recipes.ShoppingItem.user: (fields.W342) Setting unique=True on a ForeignKey has the same effect as using a OneToOneField.
        HINT: ForeignKey(unique=True) is usually better served by a OneToOneField.
```

- [x] Add model to Django admin for ease of use
- [x] Add Views/Paths
  - [x] **List**
    - [x] List of shopping items created by the user _not all user's lists_
    - [x] Template: `/templates/recipes/shopping_items/list.html`
    - [x] `''` base path
  - [x] **Create**
    - [x] Only handles POST requests, meaning no HTML template
    - [x] Should create a `ShoppingItem` instance in the db based on current user and the value of the submitted `ingredients_id` value
    - [x] Achieved this with a submit button within an empty form. Now button POSTs
    - [x] Set view to redirect to same page after successful request
    - [x] Conditionally show add button
  - [x] **Delete**
    - [x] Same as Create view, no template only POST request
    - [x] Delete `ShoppingItem` instances associated w/current user, then show the empty shopping list after deletion
- [x] Templates
  - [x] Recipe Detail Template
    - [ ] Add shopping list data to context
    - [x] Found something better! Create a `context_processor` so that the list is available globally (i.e. can be used in the header to display num of items in list)

## Post mortem

I would like to review how to setup a user login/logout. I relied on these features heavily in the project, but my memory of creating them was a bit fuzzy. Definitely first on the ol' review list.

Having the "add to list button" conditionally appear took the longest. I thought "this is getting kinda complicated, I better do this in the view instead of the template." This lead to hours of unsuccessful attempts.

Now after seeing the template help file, I adapted the syntax to my own project's setup and everything is great! This was a learning experience in where the logic capabilities of Django templates end and the view's responsibility begins.

# Steps to Adding Servings

**Desired behavior**: A user changes a form input to the desired number of servings. The ingredient amounts adjust based on the serving size.

- [x] Update Recipe model to include servings
  - [x] servings: `positivesmallintegerfield` null == True
- [x] Handle migrations
- [x] Learn from yesterday! No complex view magic required, the template can handle this logic. Just update `CreateView` and `EditView` to include input for `servings`, and make sure `DetailView` context includes the new `servings` field.
  - [x] Edit update view to add new serving value to context. No need to actually hit the db since this is only a one-time calculation
- [x] Templates

  - [x] If servings exists display on page
  - [x] Write custom template filter! (Needed "Strategy and Steps" page for this one)

    - [x] Create a `templatetags` dir at same level as app's `models.py`

      - [x] Don't forget to include `__init__.py` file
      - [x] `resize_to.py` ->

      ```python
      from django import template
      register = template.Library()

      @register.filter
      def resize_to(ingredient, servings):
      	...
      ```

  - [x] Apply template filter
